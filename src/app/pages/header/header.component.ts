import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  username: string;

  constructor(private router: Router) { }

  ngOnInit() {
    this.username = localStorage.getItem('user');
  }

  openjoblist() {
    this.router.navigate(['joblist']);
  }
  openCandidatelist() {
    this.router.navigate(['candidatelist']);
  }

}
