import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../shared-services/shared-service/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public oLogin: any = {
    email_id: '',
    password: ''
  };
  public isLoading = false;

  constructor(private router: Router, public oAuth: AuthService) { }

  ngOnInit() {
  }

  fnLogin() {
    this.isLoading = true;
    this.oAuth.login(this.oLogin).subscribe((oData) => {
      this.isLoading = false;
      if (oData.status === 'success') {
        this.router.navigate(['dashboard']);
        localStorage.setItem("user", this.oLogin.email_id)
      }
    }, (sError) => {
      this.isLoading = false;
    });
  }

}
