import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { JobsListService } from '../../../shared-services/shared-service/services/jobs-list.service';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-joblist',
  templateUrl: './joblist.component.html',
  styleUrls: ['./joblist.component.css']
})
export class JoblistComponent implements OnInit {
  showProfile = false;
  display = 'none';
  job = {
    job_title: '',
    job_unit_name: '',
    file: '',
  }
  jobDetails: any = [];
  filename: any;
  showFilesUploaded = false;
  filedata: any;
  progressVisible: boolean;
  constructor(private router: Router, private jobService: JobsListService) { }

  ngOnInit() {
    this.getJobDetails();
    $(document).ready(function () {
      $('[data-toggle="tooltip"]').tooltip();
    });


  }

  w3_open() {
    this.showProfile = true;
    this.display = 'block';
  }
  w3_close() {
    this.display = 'none';
    this.showProfile = false;
  }
  joblist() {
    this.jobService.postJobDetails(this.job).subscribe((oData) => {

      this.getJobDetails();
    }, (sError) => {
    });
    this.w3_close();
  }
  getCandidateList(job) {

    this.router.navigate(['candidatelist/' + job['_id']]);
  }
  getJobDetails() {
    this.jobService.getJobDetails().subscribe((oData) => {
      this.jobDetails = oData;
    }, (sError) => {
    });
  }
  handleFileInput(file) {
    console.log(file);
    this.showFilesUploaded = true;
    // this.percentage = (bytesLoaded / file.size) * 100;
    this.filedata = file[0];
    const fd = new FormData();
    // for (let i in file) {
      fd.append('file', file);
    // }
    const xhr = new XMLHttpRequest();
    xhr.upload.addEventListener('progress', uploadProgress, false);
    xhr.addEventListener('loadload', uploadComplete, false);
    xhr.addEventListener('error', uploadFailed, false);
    // xhr.addEventListener('abort', uploadCanceled, false)
    xhr.open('POST', 'upload/uploadfile');
    this.progressVisible = true;
    xhr.send(fd);


    function uploadProgress(evt) {
      if (evt.lengthComputable) {
        this.progress = Math.round(evt.loaded * 100 / evt.total);
      } else {
        this.progress = 'unable to compute';
      }
    }

    function uploadComplete(evt) {
      /* This event is raised when the server send back a response */
      alert(evt.target.responseText)
    }

    function uploadFailed(evt) {
      alert("There was an error attempting to upload the file.")
    }
  }

}


