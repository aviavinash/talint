import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JobsListService } from '../../../shared-services/shared-service/services/jobs-list.service';
@Component({
  selector: 'app-candidates-list',
  templateUrl: './candidates-list.component.html',
  styleUrls: ['./candidates-list.component.css']
})
export class CandidatesListComponent implements OnInit {
  sub: any;
  id: number;
  candidateData: any;
  constructor(private router: Router,private route : ActivatedRoute,private jobService :JobsListService) { }

  ngOnInit() {
    this.sub= this.route.params.subscribe(params =>{
      this.id= params['jobId'];
    });
    this.getCandidates();
  }

  openDashboard() {
    this.router.navigate(['dashboard']);
  }
  getCandidates(){
    this.jobService.getCandidateList(this.id).subscribe((oData) => {
      this.candidateData=oData.profiles;
    }, (sError) => {
    });
  }

}
