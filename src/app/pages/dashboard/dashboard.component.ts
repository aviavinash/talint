import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JobsListService} from '../../shared-services/shared-service/services/jobs-list.service'
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {


  constructor(private router: Router,private jobService: JobsListService) { }


  ngOnInit() {
    this.getJobDetails();
  }
  getJobDetails() {
    this.jobService.getJobDetails().subscribe((oData) => {
      this.jobService.jobDetails=oData;
      if (Number(oData.length) === 0) {
        this.router.navigate(['createjob']);
      } else {
        this.router.navigate(['joblist']);
      }
    }, (sError) => {
    });   

  }

}
