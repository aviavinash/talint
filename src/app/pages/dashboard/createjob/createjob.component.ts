import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { JobsListService } from '../../../shared-services/shared-service/services/jobs-list.service';
@Component({
  selector: 'app-createjob',
  templateUrl: './createjob.component.html',
  styleUrls: ['./createjob.component.css']
})
export class CreatejobComponent implements OnInit {
  showProfile = false;
  display = 'none';
  job = {
    job_title: '',
    job_unit_name: '',
    file: '',
  }

  constructor(private router: Router, private jobService: JobsListService) { }

  ngOnInit() {
  }
  w3_open() {
    this.showProfile = true;
    this.display = 'block';
  }
  w3_close() {
    this.display = 'none';
    this.showProfile = false;
  }
  
  joblist() {
    this.jobService.postJobDetails(this.job).subscribe((oData) => {
      if (oData.status === 'success') {
        this.router.navigate(['joblist']);
      }
    }, (sError) => {
    });
  }


}
