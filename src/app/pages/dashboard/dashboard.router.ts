import { Routes, RouterModule } from '@angular/router';
import { MainContentComponent } from './main-content/main-content.component';
import { HeaderComponent } from '../header/header.component';
import {DashboardComponent } from './dashboard.component';
import { CreatejobComponent } from './createjob/createjob.component';
import { JoblistComponent } from './joblist/joblist.component';
import { CandidatesListComponent } from './candidates-list/candidates-list.component';
export const defaultRoutes: Routes = [{
    path: '',
    component: DashboardComponent,
    children: [
        { path: '', redirectTo: 'dashboard' , pathMatch: 'full'},
        { path: 'dashboard',  component: MainContentComponent },
        { path: 'createjob',  component: CreatejobComponent },
        { path: 'joblist',  component: JoblistComponent },
        { path: 'candidatelist/:jobId',  component: CandidatesListComponent }
    ]
 }];

export const AppRoute = RouterModule.forRoot(defaultRoutes, { useHash: true });
