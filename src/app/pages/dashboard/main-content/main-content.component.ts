import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-main-content',
  templateUrl: './main-content.component.html',
  styleUrls: ['./main-content.component.css']
})
export class MainContentComponent implements OnInit {
  skills=[{'name':'Spark','progress':"32%"},{'name':'Hadoop','progress':"18%"},{'name':'Scale','progress':"15%"},{'name':'Hive','progress':"12%"}];
  experience=[{'exp_in_years':'0-3','progress':"32%"},{'exp_in_years':'4-6','progress':"28%"},{'exp_in_years':'7-10','progress':"20%"},{'exp_in_years':'10-14','progress':"15%"},{'exp_in_years':'15','progress':"5%"}];
  constructor(private router: Router) { }

  ngOnInit() {
  }

  createJob() {
    this.router.navigate(['createjob']);
  }

  CandidateList() {
    this.router.navigate(['candidatelist']);
  }
}
