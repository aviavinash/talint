import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MainContentComponent } from './main-content/main-content.component';
import { RouterModule, Router, Routes } from '@angular/router';
import { AppRoute } from './dashboard.router';
import { DashboardComponent } from './dashboard.component';
import { HeaderComponent } from '../header/header.component';
import { CreatejobComponent } from './createjob/createjob.component';
import { JoblistComponent } from './joblist/joblist.component';
import { CandidatesListComponent } from './candidates-list/candidates-list.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AppRoute
  ],
  declarations: [MainContentComponent, DashboardComponent, HeaderComponent, CreatejobComponent, JoblistComponent, CandidatesListComponent]
})
export class DashboardModule { }
