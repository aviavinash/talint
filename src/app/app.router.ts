import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from '../app/pages/login/login.component';

export const AppRoutes: Routes = [
{ path: '', redirectTo: 'login', pathMatch: 'full' },
{ path: 'login',  component: LoginComponent },
{ path: 'dashboard', loadChildren: './pages/dashboard/dashboard.module#DashboardModule'},

];

export const AppRouteRoot = RouterModule.forRoot(AppRoutes, { useHash: true });
