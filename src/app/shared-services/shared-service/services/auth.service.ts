import { Injectable } from '@angular/core';

import { Response, Http } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BaseService } from './base.service';
import 'rxjs/add/operator/catch';
import { CookieService } from './cookie.service';
@Injectable()
export class AuthService {

  constructor(private router: Router, private cookie: CookieService, private http: Http, private base: BaseService) {
    // super(router, cookie);
  }

  public login(oData: Object): Observable<any> {
    this.base.setContentHeaderslogin();
    return this.http.post('/signin',
      oData,
      {
        headers: this.base.getHeadersLogin(),
        withCredentials: false
      })
      .map(data => this.handleLoginSuccess(data))
      .catch((error) => Observable.throw(error));

  }
  private handleLoginSuccess(data: Response) {
    try {
      const oRtn = data.json();
      if (oRtn.token && oRtn.userId) {
        this.cookie.putObject('userInfo', oRtn);
      }
      oRtn.status = 'success';
      return oRtn;
    } catch (e) {
      return { status: 'failure', msg: 'failed' };
    }
  }


}
