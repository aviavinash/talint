import { TestBed, inject } from '@angular/core/testing';

import { JobsListService } from './jobs-list.service';

describe('JobsListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JobsListService]
    });
  });

  it('should be created', inject([JobsListService], (service: JobsListService) => {
    expect(service).toBeTruthy();
  }));
});
