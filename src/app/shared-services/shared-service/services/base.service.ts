import { Injectable, Optional } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { CookieService } from './cookie.service';
import 'rxjs/add/operator/map';

/**
 * This is the base service for all the services which does API calls.
 * It mostly handles generic success and generic errors from the APIs
 */
@Injectable()
export class BaseService {
  /**
    * Handles all the http calls
    */
  public http: Http;

  /**
   * API End-Point
   */

  public baseUrl = 'http://localhost:3000';
  /**
   * Headers which are set for every API request
   */
  public header: any;
  chartFinderStatus: any;
  /**
   *
   * @param router Used for navigations
   * @param cookie Used for cookie handling if requied
   */
  constructor(public router: Router, public cookie: CookieService, ) { this.header = new Headers(); }

  /**
     * Sets the default headers for every request
     */
  public setContentHeaderslogin(): void {
    this.header.delete('Content-Type');
    this.header.append('Content-Type', 'application/json');
  }

  public setContentHeaders(): void {
    this.header.delete('Content-Type');
    this.header.append('Content-Type', 'application/json');
  }

  public setFileContentHeaders(): void {
    this.header.delete('Content-Type');
    this.header.set('Accept', 'multipart/form-data');
  }
  /**
   * Sets the session header for every request
   */
  public setAuthHeader(): void {
    const oUser = this.cookie.getObject('userInfo');
  }

  /**
   * Sets the session header for every request
   */

  public setSessionHeader(): void {
  }

  /**
   * Returns the default headers
   */
  public getHeaders(): any {
    return this.header;
  }

  public getHeadersLogin(): any {
    this.header.delete('Access_token');
    return this.header;
  }

  /**
   * This function is to be used for all the API success responses
   * @param data The API success response
   */

  public _genericSuccess(data: Response): any {
    try {
      const oRtn = data.json();
      oRtn.status = 'success';
      return oRtn;
    } catch (e) {
      return { status: 'success', msg: 'sucessfully' };
    }

  }

  /**
   * This function is to be used for all the API error responses
   * @param error The API error response
   */

  public _genericError(error: any): Observable<any> {
    let sError = '';
    if (error.status === 401) {
      this.cookie.remove('userInfo');
      this.router.navigate(['/login', { redirect_url: btoa(window.location.hash) }]);
    } else if (error.status === 403) {
      this.cookie.remove('userInfo');
      this.router.navigate(['/login'], { queryParams: { reason: 'not_authorized' } });
    } else if (error.status === 409) {
      // this.toastr.warning('Document is already locked and Cannot be processed');
    } else if (error.status === 500 || error.status === 501 || error.status === 502 || error.status === 503) {
      this.chartFinderStatus = error.status;
    } else if (error.status === 504) {
      this.chartFinderStatus = error.status;
    }
    try {
      sError = JSON.parse(error._body);
    } catch (e) {
      sError = 'Something went wrong. Please try again!';
    }

    return Observable.throw(sError);

  }
  
}





