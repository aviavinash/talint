import { Injectable } from '@angular/core';
import { Response, Http } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BaseService } from './base.service';
import 'rxjs/add/operator/catch';
import { CookieService } from './cookie.service';
@Injectable()
export class JobsListService {
  jobDetails: any;

  constructor(private router: Router, private cookie: CookieService, private http: Http, private base: BaseService) { }

  public getJobDetails() {
    this.base.setContentHeaders();
    this.base.setAuthHeader();
    return this.http.get('users/job',
      {
        headers: this.base.getHeaders(),
        withCredentials: false,
      })
      .map(data => this.base._genericSuccess(data))
      .catch(error => this.base._genericError(error));
  }
  public postJobDetails(oData: Object): Observable<any> {
    this.base.setContentHeaders();
    this.base.setAuthHeader();
    return this.http.post('/users/job',
      oData, {
        headers: this.base.getHeaders(),
        withCredentials: false
      })
      .map(data => this.base._genericSuccess(data))
      .catch(error => this.base._genericError(error));
  }
  public getCandidateList(jobID) {
    this.base.setContentHeaders();
    this.base.setAuthHeader();
    return this.http.get('users/get/candidatedetails/'+ jobID,
      {
        headers: this.base.getHeaders(),
        withCredentials: false,
      })
      .map(data => this.base._genericSuccess(data))
      .catch(error => this.base._genericError(error));
  }
  
}
