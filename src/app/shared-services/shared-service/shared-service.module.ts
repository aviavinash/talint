import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthService } from './services/auth.service';
import { BaseService } from './services/base.service';
import { CookieService } from './services/cookie.service';
import { JobsListService} from './services/jobs-list.service';
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class SharedServicesModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedServicesModule,
      providers: [
        BaseService,
        CookieService,
        AuthService,
        JobsListService
      ]
    };
  }

}
